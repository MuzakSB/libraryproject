import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.net.URL;

/**
 * Created by Bin on 28/12/2015.
 * ToolBar with book and library operation button
 */
public class OperationTBar extends JToolBar{
    private JButton newBook, modBook, delBook, newLib, loadLib, saveLib, printLib;
    private URL nbUrl, mbUrl, dbUrl, nlUrl, llUrl, slUrl, plUrl;
    private int iconWidth = 64, iconHeight = 64;
    /*Constructor*/
    public OperationTBar(){

        setLayout(new FlowLayout(FlowLayout.CENTER));

        newBook = new JButton();
        modBook = new JButton();
        delBook = new JButton();
        newLib = new JButton();
        loadLib = new JButton();
        saveLib = new JButton();
        printLib = new JButton();

        newBook.setActionCommand("Aggiungi libro");
        modBook.setActionCommand("Modifica libro");
        delBook.setActionCommand("Elimina libro");
        newLib.setActionCommand("Nuova biblioteca");
        loadLib.setActionCommand("Carica biblioteca");
        saveLib.setActionCommand("Salva biblioteca");
        printLib.setActionCommand("Stampa biblioteca");


        newBook.setToolTipText("Aggiungi libro");
        modBook.setToolTipText("Modifica libro");
        delBook.setToolTipText("Elimina libro");
        newLib.setToolTipText("Nuova libreria");
        saveLib.setToolTipText("Salva libreria");
        loadLib.setToolTipText("Carica libreria");
        printLib.setToolTipText("Stampa biblioteca");

        newBook.setOpaque(false);
        modBook.setOpaque(false);
        delBook.setOpaque(false);
        newLib.setOpaque(false);
        loadLib.setOpaque(false);
        saveLib.setOpaque(false);
        printLib.setOpaque(false);

        nbUrl = OperationTBar.class.getResource("resources/icon/newBook.png");
        mbUrl = OperationTBar.class.getResource("resources/icon/modBook.png");
        dbUrl = OperationTBar.class.getResource("resources/icon/deleteBook.png");
        nlUrl = OperationTBar.class.getResource("resources/icon/newLibrary.png");
        llUrl = OperationTBar.class.getResource("resources/icon/loadLibrary.png");
        slUrl = OperationTBar.class.getResource("resources/icon/saveLibrary.png");
        plUrl = OperationTBar.class.getResource("resources/icon/printLibrary.png");


        newBook.setIcon(new ImageIcon(new ImageIcon(nbUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        newBook.setBorder(null);
        modBook.setIcon(new ImageIcon(new ImageIcon(mbUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        modBook.setBorder(null);
        delBook.setIcon(new ImageIcon(new ImageIcon(dbUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        delBook.setBorder(null);
        newLib.setIcon(new ImageIcon(new ImageIcon(nlUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        newLib.setBorder(null);
        loadLib.setIcon(new ImageIcon(new ImageIcon(llUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        loadLib.setBorder(null);
        saveLib.setIcon(new ImageIcon(new ImageIcon(slUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        saveLib.setBorder(null);
        printLib.setIcon(new ImageIcon(new ImageIcon(plUrl).getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
        printLib.setBorder(null);



        newBook.addActionListener(Main.librarian);
        modBook.addActionListener(Main.librarian);
        delBook.addActionListener(Main.librarian);
        newLib.addActionListener(Main.librarian);
        saveLib.addActionListener(Main.librarian);
        loadLib.addActionListener(Main.librarian);
        printLib.addActionListener(Main.librarian);

        addSeparator();
        add(newBook);
        addSeparator();
        add(modBook);
        addSeparator();
        add(delBook);
        addSeparator();
        add(newLib);
        addSeparator();
        add(loadLib);
        addSeparator();
        add(saveLib);
        addSeparator();
        add(printLib);
        addSeparator();

        setFloatable(false);
    }
}