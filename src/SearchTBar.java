/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Toolbar for searching word or string in the current library and then display the filtered JTable
 * @author Sun Bin
 */

public class SearchTBar extends JToolBar implements ActionListener {
    private JLabel searchLabel;
    private JTextField searchField;
    private JButton btnClear;

    private TableRowSorter<TableModelBook> rowSorter;

    /**
     * Constructor
     */
    public SearchTBar() {
        rowSorter = new TableRowSorter<>(Main.tm);
        Main.table.setRowSorter(rowSorter);
        searchLabel = new JLabel("Trova la parola: ");
        searchField = new JTextField();
        btnClear = new JButton("Clear");
        btnClear.addActionListener(this);
        searchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = searchField.getText();
                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = searchField.getText();
                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        add(searchLabel);
        add(searchField);
        add(btnClear);

        setFloatable(false);
    }

    /**
     * Action Listener used in this class
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Clear")) {
            searchField.setText(null);
            rowSorter.setRowFilter(null);
        }
    }
}