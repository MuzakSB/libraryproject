/* Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import nl.siegmann.epublib.epub.EpubReader;
import nl.siegmann.epublib.viewer.Viewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Open Epub file with external library
 * Called in Librarian
 * @see Librarian
 * @author Sun Bin
 */

public class ViewEpub {
    private File file;

    /**
     * Constructor
     * @param file will be open with reader
     */
    public ViewEpub(File file) {
        EpubReader epubReader = new EpubReader();
        try {
            Viewer v = new Viewer(epubReader.readEpub(new FileInputStream(file)));
        }catch (IOException e){
            System.out.print("Error message" + e.getMessage());
        }
    }

}
