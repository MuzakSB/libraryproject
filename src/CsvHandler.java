import java.io.*;
import java.util.List;

/**
 * Created by Bin on 06/02/2016.
 * Class for handling of csv file
 */

public class CsvHandler {
    //Declaring separator
    private static final String COLUMN_SEPARATOR = ";";
    private static final String ROW_SEPARATOR = "\n";
    /**
     * Write Csv file
     * Export current table information into csv file
     */
    public static void writeCsvFile (String fileName){
        try{
            File file = new File(fileName + ".csv");
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            List<Book> tempBookList = Main.tm.getBooklist();
            for(int i = 0; i < tempBookList.size(); i++){
                bw.write(tempBookList.get(i).getTitle());
                bw.write(COLUMN_SEPARATOR);
                bw.write(tempBookList.get(i).getAuthor());
                bw.write(COLUMN_SEPARATOR);
                bw.write(String.valueOf(tempBookList.get(i).getYear()));
                bw.write(COLUMN_SEPARATOR);
                bw.write(String.valueOf(tempBookList.get(i).getPages()));
                bw.write(COLUMN_SEPARATOR);
                bw.write(tempBookList.get(i).getFilePath());
                bw.write(ROW_SEPARATOR);
            }
            fw.flush();
            bw.close();
            fw.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Read Csv file
     * Import csv file and load it into Jtable
     */
    public static void readCsv (File file){
        String title;
        String author;
        int year;
        int pages;
        String path;
        String line;
        BufferedReader fileReader = null;
        try {
            fileReader = new BufferedReader(new FileReader(file));
            //Read one line, distribute it in the right place and load it to the table
            while ((line = fileReader.readLine()) != null){
                String[] tokens = line.split(COLUMN_SEPARATOR, -1);
                if (tokens.length > 0) {
                    if(!tokens[0].isEmpty()) {
                        title = tokens[0];
                    } else {
                        title = "Inserire titolo";
                    }
                    if(!tokens[1].isEmpty()) {
                        author = tokens[1];
                    } else {
                        author = "Inserire titolo";
                    }
                    if (tokens[2].matches("\\d*")){
                        year = Integer.parseInt(tokens[2]);
                    } else {
                        year = 0;
                    }
                    if (tokens[3].matches("\\d*")) {
                        pages = Integer.parseInt(tokens[3]);
                    } else {
                        pages = 0;
                    }
                    path = tokens[4];
                    Book book = new Book(title, author, year, pages, path);
                    Main.tm.newBook(book);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

