/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Open File with built in reader for epub and pdf
 * For other type let you choose which programm use
 * Called in Librarian
 * @see Librarian
 * @author Sun Bin
 */
public class OpenFile {
    private File file;
    private Desktop desktop = Desktop.getDesktop();;

    /**
     * Constructor
     * @param file passed from Librarian
     */
    public OpenFile (File file){
        this.file = file;
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(file);
            }else{
                JOptionPane.showMessageDialog(null, "Awt Desktop non supportato!");
            }
        } catch (IOException fileError){
            fileError.printStackTrace();
        }
    }
}
