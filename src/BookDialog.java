import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * JDialog for book creating book and modifying book's information
 * Called in Nuovo Libro and Modifica Libro buttons in MainPanel
 *
 * @author Sun Bin
 * @see MainPanel
 */

public class BookDialog extends JDialog implements ActionListener {
    private JPanel formPanel, buttonPanel;
    private JTextField titleTF, authorTF, yearTF, pagesTF, pathTF;
    protected JLabel titlelbl;
    private JLabel authorlbl;
    private JLabel yearlbl;
    private JLabel pageslbl;
    private JButton savebtn, cancelbtn, pathbtn;
    private int selectedRow;
    private String regex = "\\d+";

    /**
     * Constructor for creating new book
     */
    public BookDialog() {
        formPanel = new JPanel();
        buttonPanel = new JPanel();

        formPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.gridx = 0;
        gbc.gridy = 0;
        formPanel.add(titlelbl = new JLabel("Titolo: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        formPanel.add(titleTF = new JTextField(30), gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        formPanel.add(authorlbl = new JLabel("Autore: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        formPanel.add(authorTF = new JTextField(30), gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        formPanel.add(yearlbl = new JLabel("Anno pubblicazione: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        formPanel.add(yearTF = new JTextField(4), gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        formPanel.add(pageslbl = new JLabel("Pagine: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        formPanel.add(pagesTF = new JTextField(4), gbc);
        gbc.gridx = 0;
        gbc.gridy = 4;
        formPanel.add(pathbtn = new JButton("Scegli: "), gbc);
        pathbtn.addActionListener(this);
        gbc.gridx = 1;
        gbc.gridy = 4;
        formPanel.add(pathTF = new JTextField(30), gbc);

        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 30));
        buttonPanel.add(savebtn = new JButton("Salva"));
        buttonPanel.add(cancelbtn = new JButton("Cancella"));
        savebtn.setActionCommand("NuovoFile");
        savebtn.addActionListener(this);
        cancelbtn.addActionListener(this);

        setLayout(new BorderLayout());
        add(formPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        setTitle("Dettagli libro");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
    }

    /**
     * Overloading Constructor for save book. (Polymorphism)
     * Passing Parameters which are displayed in the form's
     *
     * @param title
     * @param author
     * @param year
     * @param pages
     * @param path
     * @param row
     */
    public BookDialog(String title, String author, int year, int pages, String path, int row) {
        this.selectedRow = row;
        formPanel = new JPanel();
        buttonPanel = new JPanel();

        formPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.gridx = 0;
        gbc.gridy = 0;
        formPanel.add(titlelbl = new JLabel("Titolo"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        formPanel.add(titleTF = new JTextField(title, 30), gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        formPanel.add(authorlbl = new JLabel("Autore"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        formPanel.add(authorTF = new JTextField(author, 30), gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        formPanel.add(yearlbl = new JLabel("Anno pubblicazione"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        formPanel.add(yearTF = new JTextField(String.valueOf(year), 4), gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        formPanel.add(pageslbl = new JLabel("Pagine: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        formPanel.add(pagesTF = new JTextField(String.valueOf(pages), 4), gbc);
        gbc.gridx = 0;
        gbc.gridy = 4;
        formPanel.add(pathbtn = new JButton("Scegli: "), gbc);
        pathbtn.addActionListener(this);
        gbc.gridx = 1;
        formPanel.add(pathTF = new JTextField(path, 30), gbc);

        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 30));
        buttonPanel.add(savebtn = new JButton("Salva"));
        buttonPanel.add(cancelbtn = new JButton("Cancella"));
        savebtn.setActionCommand("SalvaFile");
        savebtn.addActionListener(this);
        cancelbtn.addActionListener(this);

        setLayout(new BorderLayout());
        add(formPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        setTitle("Dettagli libro");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(new Dimension(580, 340));
        setVisible(true);
        setLocationRelativeTo(null);
        setModal(true);
        setResizable(false);
    }

    /**
     * Action listener for this class
     * the buttons associated are "Path", "CREATE", "Save" and "Cancel"
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Open filechooser for file's path
        if (e.getActionCommand().equals("Scegli: ")) {
            JFileChooser fileChooser = new JFileChooser();
            FileFilter txtFilter = new FileNameExtensionFilter("Text Files (txt)", "txt");
            FileFilter pdfFilter = new FileNameExtensionFilter("Portable Document File (PDF)", "pdf");
            FileFilter epubFilter = new FileNameExtensionFilter("Electronic Publication (Epub)", "EPub");
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(pdfFilter);
            fileChooser.addChoosableFileFilter(epubFilter);
            fileChooser.addChoosableFileFilter(txtFilter);
            fileChooser.setAcceptAllFileFilterUsed(true);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                pathTF.setText(selectedFile.getAbsolutePath());
                pathTF.setEditable(false);
            }
        }

        //Button for create new book
        if (e.getActionCommand().equals("NuovoFile")) {
            if (pagesTF.getText().matches(regex) && yearTF.getText().matches(regex)) {
                String title = titleTF.getText();
                String author = authorTF.getText();
                String year = yearTF.getText();
                String pages = pagesTF.getText();
                String path = pathTF.getText();
                Book book = new Book(title, author, Integer.parseInt(year), Integer.parseInt(pages), path);
                Main.tm.newBook(book);
                Main.isSaved = false;
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Dati inseriti non validi o mancanti", "Errore", JOptionPane.OK_OPTION);
                return;
            }
        }

        //Button for saving book
        if (e.getActionCommand().equals("SalvaFile")) {
            if (pagesTF.getText().matches(regex) && yearTF.getText().matches(regex) && !titleTF.getText().isEmpty() && !authorTF.getText().isEmpty()) {
                String title = titleTF.getText();
                String author = authorTF.getText();
                String year = yearTF.getText();
                String pages = pagesTF.getText();
                String path = pathTF.getText();
                Book book = new Book(title, author, Integer.parseInt(year), Integer.parseInt(pages), path);
                Main.tm.updateValueRow(book, selectedRow);
                Main.isSaved = false;
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Dati inseriti non validi o mancanti", "Errore", JOptionPane.OK_OPTION);
                return;
            }
        }

        //Button for nullifying operation, it also close the dialog
        if (e.getActionCommand().equals("Cancella")) {
            dispose();
        }
    }
}