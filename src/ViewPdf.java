/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PagePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Open PDF file using a viewer developed in this class
 * Called in Librarian
 * @see Librarian
 * @author Sun Bin
 */

public class ViewPdf extends JFrame implements ActionListener {
    private File file;
    private PDFFile doc;
    private PagePanel viewer;
    private PDFPage page;
    private JButton nextBnt, prevBtn;
    private JTextField currTf, pageTf;
    private JPanel buttonPanel;

    private int currentPage = 1;
    private int numberOfPages = 0;


    /**Constructor
     * @param file will be open with the library
     */
    public ViewPdf (File file) {
        this.file = file;

        //Setting Up Buttons and textfield
        buttonPanel = new JPanel();
        prevBtn = new JButton("◄◄");
        nextBnt = new JButton("►►");

        prevBtn.setOpaque(false);
        nextBnt.setOpaque(false);

        currTf = new JTextField(4);
        pageTf = new JTextField(4);

        buttonPanel.add(prevBtn);
        buttonPanel.add(currTf);
        buttonPanel.add(pageTf);
        buttonPanel.add(nextBnt);

        add(buttonPanel, BorderLayout.NORTH);


        prevBtn.addActionListener(this);
        nextBnt.addActionListener(this);
        currTf.addActionListener(this);
        currTf.setActionCommand("enter");



        //Setting up PDF viewer
        viewer = new PagePanel();
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            doc = new PDFFile(buf);
            numberOfPages = doc.getNumPages();
        } catch (IOException e) {
            System.out.print("Error: " + e.getMessage());
        }

        add(viewer, BorderLayout.CENTER);
        viewer.setVisible(true);

        //Setting Up Listener
        addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                e.consume();

                int notches = e.getWheelRotation();
                if (notches < 0) {
                    previousPage();
                } else {
                    nextPage();
                }
            }
        });

        /*Setting up action used in keymap*/
        AbstractAction prevPageAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                previousPage();
            }
        };
        AbstractAction nextPageAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nextPage();
            }
        };

        /*Setting up keymap which will be used to change page*/
        prevBtn.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "left");
        prevBtn.getActionMap().put("left", prevPageAction);
        nextBnt.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "right");
        nextBnt.getActionMap().put("right", nextPageAction);


        currTf.setText(String.valueOf(currentPage));
        pageTf.setText(String.valueOf(numberOfPages));
        pageTf.setEditable(false);

        String title = file.getName();
        setTitle(title);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        viewPage(currentPage);
    }

    /**
     * Method for painting painting the page
     * @param num is which page will be painted
     */
    private void viewPage(int num){
        page = doc.getPage(num);
        viewer.showPage(page);
        currTf.setText(String.valueOf(currentPage));
    }

    /**
     * Method to increase current page counter and then show the page
     */
    private void nextPage (){
       if (currentPage < numberOfPages){
            currentPage ++;
            viewPage(currentPage);
        }
    }

    /**
     * Method to decrease current page counter and then show the page
     */
    private void previousPage(){
        if (currentPage > 1){
            currentPage --;
            viewPage(currentPage);
        }
    }

    /**
     * Action listerner for this class
     * The buttons associated are left arrows and right arrows and "enter"
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("◄◄")){
            previousPage();
        }
        if (e.getActionCommand().equals("►►")){
            nextPage();
        }
        if (e.getActionCommand().equals("enter")){
            if (currTf.getText().matches("[1-9][0-9]*")){
                int num = Integer.parseInt(currTf.getText());
                if (num <= numberOfPages){
                    currentPage = num;
                    viewPage(currentPage);
                } else {
                    currTf.setText(String.valueOf(currentPage));
                }
            } else {
                currTf.setText(String.valueOf(currentPage));
            }
        }
    }
}
