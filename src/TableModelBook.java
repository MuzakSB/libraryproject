/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Default table model for management of Jtable
 * @see Librarian
 * @author Sun Bin
 */
public class TableModelBook extends DefaultTableModel implements TableModelListener{
    private List<Book> bookList;
    /**
     * Recalling parent constructor
     * @param data
     * @param columnNames
     */
    public TableModelBook(Object[][]data, String[] columnNames){
        super(data,columnNames);
        bookList = new ArrayList<Book>();
    }

    /**
     * Convert book into object
     * @param book
     */
    public Object[] booktoObj (Book book){
        Object[] oBook = {book.getTitle(), book.getAuthor(), book.getYear(), book.getPages(), book.getFilePath()};
        return oBook;
    }

    /**
     * Add book to List
     * @param book
     */
    public void newBook (Book book){
        bookList.add(book);
        addRow(booktoObj(book));
    }

    /**
     * Update one element of the list
     * @param book from the selected row
     * @param row usually is selected row from MainPanel
     */
    public void updateValueRow (Book book, int row){
        bookList.set(row, book);
        setValueAt(book.getTitle(), row, 0);
        setValueAt(book.getAuthor(), row, 1);
        setValueAt(book.getYear(), row, 2);
        setValueAt(book.getPages(), row, 3);
        setValueAt(book.getFilePath(), row, 4);
    }

    /**
     * Method that return required object book
     * @param row
     * @return object Book
     */
    public Book getValueAt(int row){
        return bookList.get(row);
    }

    /**
     * Method that return active list of book objects
     * @return list of book object
     */
    public List<Book> getBooklist(){
        return bookList;
    }
    /**
     * Clean the list for new usage
     */
    public void clearTable (){
        bookList.clear();
        setRowCount(0);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
    }
}