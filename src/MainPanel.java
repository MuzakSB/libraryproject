/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;


/**
 * Split Panel with two Panel on right and left.
 * The left one display a JTable where current library's information is displayed
 * The right one display selected book information and open directory and file button.
 * @author Sun Bin
 */
public class MainPanel extends JSplitPane {

    //LeftPane object
    private JScrollPane leftPanel = new JScrollPane();

    //RightPane object
    private JPanel rightPanel = new JPanel();
    private JLabel lblTitle, lblAuthor, lblPublished, lblPages;
    private JButton openFile, openPath;
    private String path;

    //Constructor
    public MainPanel (){
        super();
        //Setting up leftPanel
        leftPanel.setViewportView(Main.table);

        //Setting up rightPanel
        lblTitle = new JLabel("Titolo: ");
        lblAuthor = new JLabel("Autore: ");
        lblPublished = new JLabel("Data pubblicazione: ");
        lblPages = new JLabel("Pagine: " );
        openFile = new JButton("Apri file");
        openPath = new JButton("Apri cartella");
        openFile.addActionListener(Main.librarian);
        openPath.addActionListener(Main.librarian);
        openFile.setEnabled(false);
        openPath.setEnabled(false);


        //Getting information and display it when selecting row
        Main.table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                openFile.setEnabled(!lsm.isSelectionEmpty());
                openPath.setEnabled(!lsm.isSelectionEmpty());
                if (!e.getValueIsAdjusting()){
                    if (Main.table.getSelectedRow() != -1) {
                        lblTitle.setText("Titolo: " + Main.table.getValueAt(Main.table.getSelectedRow(), 0));
                        lblAuthor.setText("Autore: " + Main.table.getValueAt(Main.table.getSelectedRow(), 1));
                        lblPublished.setText("Data pubblicazione: " + Main.table.getValueAt(Main.table.getSelectedRow(), 2));
                        lblPages.setText("Pagine: " + Main.table.getValueAt(Main.table.getSelectedRow(), 3));
                        path = (String) Main.table.getValueAt(Main.table.getSelectedRow(), 4);
                        if (!path.isEmpty()){
                            openFile.setEnabled(true);
                            openPath.setEnabled(true);
                            openFile.putClientProperty("path", Main.table.getValueAt(Main.table.getSelectedRow(), 4));
                            openPath.putClientProperty("path", Main.table.getValueAt(Main.table.getSelectedRow(), 4));
                        }
                        else {
                            openFile.setEnabled(false);
                            openPath.setEnabled(false);
                        }
                    }
                    else {
                        lblTitle.setText("Titolo: ");
                        lblAuthor.setText("Autore: ");
                        lblPublished.setText("Data pubblicazione: ");
                        lblPages.setText("Pagine: ");
                    }
                }
            }
        });

        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.add(Box.createRigidArea(new Dimension(30,70)));
        lblTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
        lblAuthor.setAlignmentX(Component.LEFT_ALIGNMENT);
        lblPublished.setAlignmentX(Component.LEFT_ALIGNMENT);
        lblPages.setAlignmentX(Component.LEFT_ALIGNMENT);
        openPath.setAlignmentX(Component.LEFT_ALIGNMENT);
        openFile.setAlignmentX(Component.LEFT_ALIGNMENT);
        rightPanel.add(lblTitle);
        rightPanel.add(Box.createRigidArea(new Dimension(30,5)));
        rightPanel.add(lblAuthor);
        rightPanel.add(Box.createRigidArea(new Dimension(30,5)));
        rightPanel.add(lblPublished);
        rightPanel.add(Box.createRigidArea(new Dimension(30,5)));
        rightPanel.add(lblPages);
        rightPanel.add(Box.createRigidArea(new Dimension(30,10)));
        rightPanel.add(openFile);
        rightPanel.add(Box.createRigidArea(new Dimension(30,5)));
        rightPanel.add(openPath);

        leftPanel.setMinimumSize(new Dimension(400,400));
        rightPanel.setMinimumSize(new Dimension(200,400));

        setRightComponent(rightPanel);
        setLeftComponent(leftPanel);

        setDividerSize(3);
    }
}
