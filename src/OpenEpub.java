import nl.siegmann.epublib.epub.EpubReader;
import nl.siegmann.epublib.viewer.Viewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


/**
 * Created by muzak on 2/23/16.
 */

/**
 * Open Epub file with external library
 * @param file is the file to read
 */

//todo: resolve bindings
public class OpenEpub {
    private File file;

    /*Constructor*/
    public OpenEpub(File file) {
        this.file = file;
        EpubReader epubReader = new EpubReader();
        try {
            Viewer v = new Viewer(epubReader.readEpub(new FileInputStream(file)));
        }catch (IOException e){
            System.out.print("Error message" + e.getMessage());
        }
    }
}
