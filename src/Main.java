/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Main Class for Library Project
 * It constructs the gui frame calling other classes.
 * It also declares some static class used in other classes.
 * Library Project is used to regroup books under library.
 * It allows you to add, modify, delete, open directory and file of books.
 * It also allows you to create, load and save library in csv file and print it.
 * @see MainPanel
 * @see SearchTBar
 * @see OperationTBar
 * @see TableModelBook
 * @author Sun Bin
 */
public class Main extends JFrame {
    //Declaring column names and null data
    private String[] columnNames = new String[]{"Titolo", "Autore", "Data Pubblicazione", "Pagine", "Percorso"};
    private Objects[][] data = {};

    public static TableModelBook tm;
    public static JTable table;
    public static boolean isSaved = true;
    public static Librarian librarian = new Librarian();

    private MainPanel mainPanel;
    private OperationTBar tbar;
    private SearchTBar stb;

    /*Constructor*/
    public Main(){
        tm = new TableModelBook(data, columnNames);
        table = new JTable(tm);
        mainPanel = new MainPanel();
        tbar = new OperationTBar();
        stb = new SearchTBar();

        mainPanel.setDividerLocation(0.60);
        mainPanel.setResizeWeight(0.60);
        add(mainPanel, BorderLayout.CENTER);
        add(tbar, BorderLayout.NORTH);
        add(stb, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if (!isSaved){
                    int response = JOptionPane.showConfirmDialog(null,"Le modifiche non sono state salvate. " +
                            "Sei sicuro di voler chiudere?","Attenzione" ,JOptionPane.YES_NO_OPTION);
                    if(response == JOptionPane.YES_OPTION) {
                        dispose();
                    }
                } else {
                    dispose();
                }
            }
        });
    }

    /*Main*/
    public static void main (String[] args){
        Main M = new Main();
        LinkedList<String> generics = null;
        M.setTitle("Library Project");
        M.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        M.pack();
        M.setVisible(true);
        M.setLocationRelativeTo(null);
    }
}