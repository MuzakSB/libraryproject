/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.File;

/**
 * Class that regroup action listener for OperationTbar, MainPanel.
 * @see OperationTBar
 * @see MainPanel
 * @author Sun Bin
 */

public class Librarian implements ActionListener {

    /**
     * Setting up action listener for the buttons in OperationTBar and MainPanel
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        /**
         * Add Book button
         * Button from OperarionTbar
         * On Click add new book in the current table
         */
        if (e.getActionCommand().equals("Aggiungi libro")) {
            BookDialog newBook = new BookDialog();
            newBook.pack();
            newBook.setLocationRelativeTo(null);
            newBook.setVisible(true);
        }

        /**
         * Change book information Button
         * Button from OperarionTbar
         * On click take information from the selected row and load it into dialog
         */
        if (e.getActionCommand().equals("Modifica libro")) {
            if (Main.table.getRowCount() > 0 ){
                if(!Main.table.getSelectionModel().isSelectionEmpty()) {
                    int row = Main.table.convertRowIndexToModel(Main.table.getSelectedRow());
                    Book tempBook= Main.tm.getValueAt(row);
                    String title = tempBook.getTitle();
                    String author = tempBook.getAuthor();
                    int year = tempBook.getYear();
                    int pages = tempBook.getPages();
                    String path = tempBook.getFilePath();
                    BookDialog modifyBook = new BookDialog(title, author, year, pages, path, row);
                    modifyBook.pack();
                    modifyBook.setVisible(true);
                    modifyBook.setLocationRelativeTo(null);
                    Main.table.clearSelection();
                } else {
                    JOptionPane.showMessageDialog(null, "Nessuna riga selezionata.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nessuna libreria attiva.");
            }
        }

        /**
         * Delete Book button
         * Button from OperationTbar
         * On click delete selected row
         */
        if (e.getActionCommand().equals("Elimina libro")) {
            if (Main.table.getRowCount() > 0 ) {
                if(!Main.table.getSelectionModel().isSelectionEmpty()){
                    int row = Main.table.convertRowIndexToModel(Main.table.getSelectedRow());
                    Main.tm.removeRow(row);
                    Main.isSaved = false;
                } else {
                    JOptionPane.showMessageDialog(null, "Nessuna riga selezionata.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nessuna libreria attiva.");
            }
        }

        /**
         * New Library button
         * Button in OperarionTbar
         * On Click create a new table to store books
         * With controller on the current table if is saved or not
         */
        if (e.getActionCommand().equals("Nuova biblioteca")){
            if (!Main.isSaved){
                int response = JOptionPane.showConfirmDialog(null,"Le modifiche non sono state salvate. Vuoi Continuare?","Attenzione" ,JOptionPane.YES_NO_OPTION);
                if(response != JOptionPane.YES_OPTION)
                    return;
            }
            Main.tm.clearTable();
            Main.isSaved = true;
        }

        /**
         * Load library button
         * Button in OperarionTbar
         * On click load a csv file from filechooser
         * with controller on the current table if is saved or not
         */

        if(e.getActionCommand().equals("Carica biblioteca")){
            if (!Main.isSaved){
                int response = JOptionPane.showConfirmDialog(null,"Le modifiche non sono state salvate. Vuoi Continuare?","Attenzione" ,JOptionPane.YES_NO_OPTION);
                if(response != JOptionPane.YES_OPTION)
                    return;
            }
            JFileChooser fileChooser = new JFileChooser();
            FileFilter csvFilter = new FileNameExtensionFilter("Comma Separated Values (csv)", "csv");
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(csvFilter);
            int result = fileChooser.showOpenDialog(null);
            if(result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                Main.tm.clearTable();
                CsvHandler.readCsv(selectedFile);
            }
            Main.isSaved = true;
        }

        /**
         * Save library button
         * Button in OperarionTbar
         * On click open file chooser to select where to save file
         * With dialog when overwriting file
         */
        if(e.getActionCommand().equals("Salva biblioteca")){
            JFileChooser fileChooser = new JFileChooser();
            FileFilter csvFilter = new FileNameExtensionFilter("Comma Separated Values (csv)", "csv");
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.addChoosableFileFilter(csvFilter);
            int result = fileChooser.showSaveDialog(null);
            if(result == JFileChooser.APPROVE_OPTION){
                File selectedFile = fileChooser.getSelectedFile();
                if(selectedFile.exists() && (selectedFile != null)) {
                    int response = JOptionPane.showConfirmDialog(fileChooser, "Il file esiste gia', vuoi sostituirlo?","File esistente",JOptionPane.YES_NO_OPTION);
                    if(response != JOptionPane.YES_OPTION){
                        Main.isSaved = false;
                        return;
                    }
                }
                String fileName = selectedFile.getAbsolutePath();
                //Regex to filter extension
                fileName = fileName.replaceFirst("[.][^.]+$", "");
                CsvHandler.writeCsvFile(fileName);
                Main.isSaved = true;
            }
        }

        /**
         * Print library button
         * Button from MainPanel
         * On click call system printer and print the current library (table)
         * Using Standard print dialog
         *
         */
        if(e.getActionCommand().equals("Stampa biblioteca")){
            try{
                boolean complete = Main.table.print();
                if (complete){
                    JOptionPane.showMessageDialog(null, "Stampa effetuata con successo!", "Stampante", JOptionPane.OK_OPTION);
                }else {
                    JOptionPane.showMessageDialog(null, "Stampa non riuscita", "Stampante", JOptionPane.OK_OPTION);
                }
            }catch (PrinterException pe){
                System.out.print("Error printinting" + pe.getMessage());
            }

        }

        /**
         * Open File button
         * Button in MainPanel
         *
         */
        if(e.getActionCommand().equals("Apri file")) {
            String path = (String) ((JButton) e.getSource()).getClientProperty("path");
            File file = new File (path);
            if(file.isFile()) {
                if(path.endsWith(".epub")){
                    ViewEpub viewEpub = new ViewEpub(file);
                }
                if(path.endsWith(".pdf")) {
                    ViewPdf viewPdf = new ViewPdf(file);
                }else{
                    OpenDir openDir = new OpenDir(file);
                }
            }else{
                JOptionPane.showMessageDialog(null, "File or Dir inesistente");
                return;
            }
        }

        /**
         * Open Directory button
         * Button in MainPanel
         * On click if selected file's directory exist open that directory
         */
        if(e.getActionCommand().equals("Apri cartella")){
            OpenDir opendir;
            String path = (String) ((JButton) e.getSource()).getClientProperty("path");
            File dir = new File (path);
            if(dir.isFile()) {
                dir  = new File (path.substring(0,path.lastIndexOf(File.separator)));
                opendir = new OpenDir(dir);
            } else  {
                JOptionPane.showMessageDialog(null, "File or Dir inesistente");
                return;
            }
        }
    }
}