/*
 Library is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 Developed by: Sun Bin [sbmuzak [at] hotmail [dot] it]
 for University as Programmazione Oggetti test project
 */

/**
 * Definin Book as class used in Librarian Class
 * @see Librarian
 * @author Sun Bin
 */
public class Book {
    private String title;
    private String author;
    private int year;
    private int pages;
    private String filePath;

    /*Constructor*/
    public Book (String title, String author, int year, int pages, String filePath){
        this.title = title;
        this.author= author;
        this.year = year;
        this.pages = pages;
        this.filePath = filePath;
    }

    /**
     * Set Title 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * SetAuthor 
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Set Year 
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Set Pages 
     */
    public void setPages(int pages) {
        this.pages = pages;
    }

    /**
     * Set FilePath 
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Get Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get Author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Get Year
     */
    public int getYear() {
        return year;
    }

    /**
     * Get Pages
     */
    public int getPages() {
        return pages;
    }

    /**
     * Get FilePath
     */
    public String getFilePath() {
        return filePath;
    }
}